CC      = gcc
CFLAGS  = -Wall -std=c99 -g -D _DEFAULT_SOURCE
LFLAGS  = -Wall -lcurses -g

SRCDIR  = src
OBJDIR  = obj
BINDIR  = bin

SOURCES := $(wildcard $(SRCDIR)/*.c)
OBJECTS := $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

all: game
game: $(OBJECTS)
	$(CC) $^ $(LFLAGS) -o $@

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@mkdir -p obj
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	@rm -rf game $(OBJDIR)
