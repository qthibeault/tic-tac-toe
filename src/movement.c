#include <curses.h>

#include "movement.h"

unsigned short get_new_selected_cell(unsigned short current_cell, int key)
{
    if (current_cell > 8)
        return current_cell;

    switch(key) {
        case KEY_UP:
            if (current_cell / 3 > 0) return current_cell - 3;
            return current_cell;
        case KEY_DOWN:
            if (current_cell / 3 < 2) return current_cell + 3;
            return current_cell;
        case KEY_LEFT:
            if (current_cell % 3 > 0) return current_cell - 1;
            return current_cell;
        case KEY_RIGHT:
            if (current_cell % 3 < 2) return current_cell + 1;
            return current_cell;
        default:
            return current_cell;
    }
}
