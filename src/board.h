#ifndef __BOARD_H__
#define __BOARD_H__

#define ROWS 3
#define COLS 3

void get_coordinates(unsigned short, unsigned short*, unsigned short*);

#endif
