#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>

#include "board.h"
#include "movement.h"

#define W_OFFSET 2

int main(void)
{
    int term_width = 0;
    int term_height = 0;

    initscr();
    noecho();
    curs_set(FALSE);
    keypad(stdscr, TRUE);
    nodelay(stdscr, TRUE);
    getmaxyx(stdscr, term_height, term_width);

    WINDOW* game_win = newwin(term_height, term_width / 2, 0, 0);
    wborder(game_win, 0, 0, 0, 0, 0, 0, 0, 0);

    // WINDOW* text_win = newwin(term_height, term_width / 2, 0, term_width / 2 + 1);
    unsigned short selected_cell = 0;
    int cell_height = (term_height - W_OFFSET) / 3;
    int cell_width = (term_width / 2 - W_OFFSET) / 3;

    while (1) {
        int input_key = 0;
        unsigned short new_selected_cell = selected_cell;

        // handle input
        if ((input_key = getch()) != ERR) {
            switch (input_key) {
                case KEY_UP:
                case KEY_DOWN:
                case KEY_LEFT:
                case KEY_RIGHT:
                    new_selected_cell = get_new_selected_cell(selected_cell, input_key);
                    break;
                default:
                    break;
            }
        }

        // Render horizontal bars
        for (int i = 1; i < 3; ++i) {
            for (int x = W_OFFSET; x < term_width / 2 - W_OFFSET; ++x) {
                mvwaddch(game_win, 1 + (i * cell_height), x, '-');
            }
        }

        // Render vertical bars
        for (int i = 1; i < 3; ++i) {
            for (int y = W_OFFSET; y < term_height - W_OFFSET; ++y) {
                mvwaddch(game_win, y, 1 + (i * cell_width), '|');
            }
        }

        unsigned short cell_x = 0, cell_y = 0;
        get_coordinates(selected_cell, &cell_x, &cell_y);

        unsigned short new_cell_x = 0, new_cell_y = 0;
        get_coordinates(new_selected_cell, &new_cell_x, &new_cell_y);

        // Clear background highlight for previous cell and render background highlight for new cell
        for (int y_pos = W_OFFSET; y_pos < cell_height + 1; ++y_pos) {
            for (int x_pos = W_OFFSET + 1; x_pos < cell_width; ++x_pos) {
                mvwaddch(game_win, y_pos + (cell_y * cell_height), x_pos + (cell_x * cell_width), ' ' | A_NORMAL);
                mvwaddch(game_win, y_pos + (new_cell_y * cell_height), x_pos + (new_cell_x * cell_width), ' ' | A_STANDOUT);
            }
        }

        selected_cell = new_selected_cell;
        wrefresh(game_win);
        usleep(3000);
    }
    endwin();

    return EXIT_SUCCESS;
}
