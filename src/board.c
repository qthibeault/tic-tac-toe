#include "board.h"

void get_coordinates(unsigned short cell, unsigned short *x, unsigned short *y)
{
    if (cell > 8) return;

    *x = cell % COLS;
    *y = cell / ROWS;
}
