# Tic Tac Toe Game
Quinn Thibeault: <quinn.thibeault96@gmail.com>

## About project
This project is to write code in C, which is my favorite language, as well as
learn a new library NCurses and develop a network application protocol that
facilitates player discovery, move broadcasting and chatting.

## Prerequisites
In order to build this application, ensure you have `make`, `gcc`, `libcurses5`
and `libcurses5-dev` installed.

## Building the application
To build the application, simply run `make` or `make all`. This will generate a
binary named `game` in the top directory of the project. You can then run the
executable and open the game
